package SparkSqlAnalysis

import java.util.concurrent.Callable
import SparkSqlAnalysis.SQL.Task
import org.apache.spark.sql.SQLContext
import util.{ConnectPoolUtil, SqlUtil}
import scala.collection.mutable.ArrayBuffer

//记得每次分析后向Mysql中插入分析结果时，都插入一列“插入时间”
//每次在本类中加入一个SQL，记得加在下面的getTaskList()方法中
object SQL {
  case class Task(tableName: String, selections: String, sql: String)

/****************************************************************************************************************/
  //查询这一批数据中“首汽约车”是最不具竞争力的厂商的订单信息，也即在这批数据中，相同起点、终点、相同时间段【年-月-日-时】、相同车型，“首汽约车”价格最高
  //首先查询出“相同起点、终点、相同时间段、相同车型”中价格最高的信息，然后join，最后过滤
  val tableName1 = "result1"
  val selections1 = "cityname, beginaddr, endaddr, cartype, brandname, estimateprice, estimatedistance, estimatetimelength, usetime"
  val sql1 = "SELECT ttt.cityname, ttt.beginaddr, ttt.endaddr, ttt.cartype, ttt.brandname, ttt.estimateprice, ttt.estimatedistance, ttt.estimatetimelength, ttt.usetime " +
    " FROM test ttt JOIN " +
    " (SELECT substr(usetime, 1,13) as usetime, beginaddr, endaddr, cartype, MAX(estimateprice) AS max_price FROM test GROUP BY substr(usetime,1,13), beginaddr, endaddr, cartype)sss " +
    " ON substr(ttt.usetime,1,13)=sss.usetime AND ttt.beginaddr=sss.beginaddr AND ttt.endaddr=sss.endaddr AND ttt.cartype=sss.cartype " +
    " AND ttt.estimateprice=sss.max_price WHERE ttt.brandname='首汽约车' "

/****************************************************************************************************************/
  //查询每一批数据中“首汽约车”在每个城市的订单数量、平均价格（名次在展示时查询Mysql计算）
  val tableName2 = "result2"
  val selections2 = "cityname, amount, avg_price"
  val sql2 = "SELECT cityname, count(*) AS amount, ROUND(AVG(estimateprice),2) AS avg_price " +
    "FROM test WHERE brandName = '首汽约车' GROUP BY cityname"

/****************************************************************************************************************/
  def getTaskList(): ArrayBuffer[Task] ={
    var array = ArrayBuffer[Task]()
    array += Task(tableName1, selections1, sql1)
    array += Task(tableName2, selections2, sql2)
    array
  }
}

class SqlThread(task: Task, date: java.util.Date, sqlContext: SQLContext) extends Callable[Any] {
  override def call(): Unit = {
    val list = sqlContext.sql(task.sql).collect()
    if (list.length > 0) {
      val conn = ConnectPoolUtil.getConnection()
      val statement = conn.createStatement()  //如果上面建立连接失败的话，这里会报空指针异常。
      statement.execute(s"TRUNCATE TABLE ${task.tableName}")
      val sb = new StringBuffer()
      sb.append(s"insert into ${task.tableName}(insert_time, ${task.selections}) values ")
      sb.append(SqlUtil.getValuesSentenceWithTime(list, date))
      println("SQL语句=" + sb.toString)
      statement.execute(sb.toString)
    }
  }
}