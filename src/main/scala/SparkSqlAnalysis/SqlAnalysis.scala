package SparkSqlAnalysis

import java.util.concurrent.{Executors, TimeUnit}
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SQLContext
import org.apache.spark.streaming.dstream.InputDStream
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.{Seconds, StreamingContext}
import util.ZookeeperUtil

object SqlAnalysis {
  val KAFKA_BROKERS = "192.168.100.51:9092,192.168.100.52:9092,192.168.100.53:9092"
  val ZOOKEEPER_HOSTS_PORTS = "192.168.100.51:2181,192.168.100.52:2181,192.168.100.53:2181"
  val CLEAN_TOPIC = "ctrip_clean"
  val GROUP_ID = "realtime-consumer-test"
  val NAMESPACE = "consumers"
  val GLOBAL_PATH = s"/${GROUP_ID}/offsets"
  val PARTITION = List(0, 1, 2)
  val TIME_INTERVAL = 60
  val MAX_TIME_WAIT = 60  //允许的线程最迟执行时间

  def main(args: Array[String]): Unit = {
    val ZK_CLIENT = ZookeeperUtil.getZKClient(ZOOKEEPER_HOSTS_PORTS, NAMESPACE)
    ZK_CLIENT.start()
 
    val conf = new SparkConf().setAppName("SparkSQL任务")
      .set("spark.dynamicAllocation.enabled", "false")
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      // 确保任务在关闭的时候优雅的关闭，即完成一个完整的batch之后再关闭
      .set("spark.streaming.stopGracefullyOnShutdown", "true")
      // 加入反压机制，自动调整每次读取的数据量
      .set("spark.streaming.backpressure.enabled", "true")
      //序列化后的rdd需要暂存在内存中
      .set("spark.kryoserializer.buffer.max", "64m")
      .set("spark.kryoserializer.buffer", "32m")
      //对目标topic每个partition、每秒钟、拉取的数据条数。（限制每秒、每个消费线程、读取每个kafka分区最大的数据量【Spark端限速，非Kafka端限速】）
      .set("spark.streaming.kafka.maxRatePerPartition","25")//也即拉取间隔为10秒的话，总数据量=10*这个值
      //当程序第一次启动，限制第一次批处理应该消费的数据，因为程序冷启动 队列里面有大量积压，防止第一次全部读取，造成系统阻塞
      .set("spark.streaming.backpressure.initialRate", "300")
      //25 * 60 * 3 = 4500数据量

    val kafkaParams = Map[String, Object](
      "group.id" -> GROUP_ID,
      "bootstrap.servers" -> KAFKA_BROKERS,
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "auto.offset.reset" -> "latest",
      "enable.auto.commit" -> (false: java.lang.Boolean)
    )
    ZookeeperUtil.ensureZKPathExists(GLOBAL_PATH, ZK_CLIENT)
    ZookeeperUtil.ensureZKPathExists(s"${GLOBAL_PATH}/${CLEAN_TOPIC}", ZK_CLIENT)
    val streamingContext = new StreamingContext(conf, Seconds(TIME_INTERVAL))
    val fromOffsets = ZookeeperUtil.getFromOffset(GLOBAL_PATH, CLEAN_TOPIC, PARTITION, ZK_CLIENT)
    val kafkaStream: InputDStream[ConsumerRecord[String, String]]= KafkaUtils.createDirectStream(streamingContext, LocationStrategies.PreferConsistent, ConsumerStrategies.Subscribe(Array(CLEAN_TOPIC), kafkaParams, fromOffsets))
    val taskList = SQL.getTaskList()//获取要执行的SQL分析任务

    kafkaStream.foreachRDD( rdd => {
      if (rdd.isEmpty()){
        println("RDD是空的--------------------------------------------------------")
      } else {  //对干净数据进行SparkSQL分析
        val date = new java.util.Date() //插入时间以分析时间为准，不使用数据库的now()函数
        val sqlContext = SQLContextSingleton.getInstance(rdd.sparkContext)
        import sqlContext.implicits._

        //如果加上collect()没法调用toDF()
        val dataFrame =
          rdd.map(KV => KV.value())
             .map(line => {
              val words = line.split("\t")
              CleanLog(Integer.valueOf(words(0)), words(1), words(2), words(3), words(4), words(5), words(6), words(7), words(8),
                java.lang.Double.valueOf(words(9)), java.lang.Double.valueOf(words(10)), java.lang.Double.valueOf(words(11)))
          }).toDF()

        dataFrame.createOrReplaceTempView("test")

        //重构为多线程分析模式。【是否真的可以实现并行执行多个sql分析尚未确定】
        val es = Executors.newFixedThreadPool(taskList.length)
        //insert_time以处理时间为准，不以插入时间为准。
        val list = new java.util.ArrayList[SqlThread]
        //为每一个RDD中的【每一个向MySQL插入分析结果的SQL语句，也即分析结果的集合】创建一个connection。
        //不采用一条分析结果一个connection，这样短期并发较高、会导致连接数据库失败。
        //也不采用一个RDD内的多个SQL插入语句共用一个connection，因为connection对象底层是加锁了的，一个线程使用它的时候，其它线程必须等待，效率低
        //同时也不能把connection对象创建为全局的（即放在foreachRDD外面），因为connection对象这个实例无法跨机器使用。
        taskList.foreach(task => list.add(new SqlThread(task, date, sqlContext)))
        es.invokeAll(list, MAX_TIME_WAIT, TimeUnit.SECONDS)
        es.shutdown()
        // 存储新的offset
        ZookeeperUtil.storeOffsets(rdd.asInstanceOf[HasOffsetRanges].offsetRanges, GLOBAL_PATH, ZK_CLIENT)
      }
    })

    streamingContext.start()
    streamingContext.awaitTermination()
  }
}

case class CleanLog(id: Int, channelName: String, cityName: String, productName: String, useTime: String,
                    beginAddr: String, endAddr: String, carType: String, brandName: String,
                    estimatePrice: Double, estimateDistance: Double, estimateTimeLength: Double)

object SQLContextSingleton {
  @transient  private var instance: SQLContext = _
  def getInstance(sparkContext: SparkContext): SQLContext = {
    if (instance == null) {
      instance = new SQLContext(sparkContext)
    }
    instance
  }
}