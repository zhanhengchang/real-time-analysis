package util

object SqlUtil {
  def getValuesSentenceWithTime(array: Array[org.apache.spark.sql.Row], date: java.util.Date): String ={
    val sb = new StringBuffer()
    for (i <- 0 until array.length) {
      val x = array(i).toString()
      val m = x.substring(1, x.length-1)//集合toString后两边有方括号
      val words = m.split(",")
      sb.append("('")
      sb.append(TimeUtil.getTime(date))
      sb.append("', ")
      for (j <- 0 until words.length) {
        sb.append("'")
        sb.append(words(j))
        sb.append("'")    //将要传入的值转换为字符串，防止包含SQL特殊符号
        if (j != words.length-1) {
          sb.append(", ")
        }
      }
      sb.append(")")
      if (i != array.length-1) {
        sb.append(", ")
      }
    }
    sb.toString
  }
}