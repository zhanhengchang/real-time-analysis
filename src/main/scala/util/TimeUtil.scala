package util

import java.text.SimpleDateFormat

object TimeUtil {
  val sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

  def getTime(date: java.util.Date): String ={
    sdf.format(date)
  }
}