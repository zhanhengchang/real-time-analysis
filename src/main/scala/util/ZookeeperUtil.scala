package util

import org.apache.curator.framework.{CuratorFramework, CuratorFrameworkFactory}
import org.apache.curator.retry.ExponentialBackoffRetry
import org.apache.kafka.common.TopicPartition
import org.apache.spark.streaming.kafka010.OffsetRange
import scala.collection.JavaConversions._
import scala.collection.mutable

object ZookeeperUtil {
  private var client: CuratorFramework = null

  /**
   * 获取Zookeeper客户端
   * @param hosts Zookeeper集群IP地址和端口号
   * @param namespace 要监听的命名空间
   * @return
   */
  def getZKClient(hosts: String, namespace: String): CuratorFramework ={
    if (client == null) {
      client = CuratorFrameworkFactory
        .builder
        .connectString(hosts)
        .retryPolicy(new ExponentialBackoffRetry(1000, 3))
        .namespace(namespace)
        .build()
    }
    client
  }

  /**
   * 确保路径存在，不存在则创建
   * @param path
   * @param client
   * @return
   */
  def ensureZKPathExists(path: String, client: CuratorFramework)={
    if (client.checkExists().forPath(path) == null) {
      client.create().creatingParentsIfNeeded().forPath(path)
    }
  }

  /**
   *保存offset
   * @param offsetRange
   * @param path  Topic的父路径
   * @param client Zookeeper客户端
   */
  def storeOffsets(offsetRange: Array[OffsetRange], path: String, client: CuratorFramework) = {
    for (o <- offsetRange){
      val zkPath = s"${path}/${o.topic}/${o.partition}"
      ZookeeperUtil.ensureZKPathExists(zkPath, client)
      client.setData().forPath(zkPath, o.untilOffset.toString.getBytes())
      println("---Offset写入ZK------\nTopic：" + o.topic +", Partition:" + o.partition + ", Offset:" + o.untilOffset)
      println("重新写入这次的偏移量是="+o.untilOffset.toString)
    }
  }

  /**
   * 创建节点并初始化节点数据
   * @param path
   * @param data
   * @param client
   */
  def createNode(path: String, data: String, client: CuratorFramework): Unit ={
    client.create().creatingParentsIfNeeded().forPath(path)
    client.setData().forPath(path, data.getBytes())
  }

  /**
   * 获取指定路径下的、指定Topic的各分区偏移量
   * @param parentPath 主题Topic的父节点路径
   * @param topic
   * @param partitionList 分区列表
   * @param client
   * @return
   */
  def getFromOffset(parentPath: String, topic: String, partitionList: List[Int], client: CuratorFramework): Map[TopicPartition, Long] = {
    val zkTopicPath = s"${parentPath}/${topic}"
    println("zkTopicPath="+zkTopicPath)
    var childrens = client.getChildren().forPath(zkTopicPath) // 获取topic的子节点，即 分区
    if(childrens.isEmpty) {
      partitionList.foreach(x => ZookeeperUtil.createNode(s"${zkTopicPath}/${x}", "0", client))
      println("初始化ZK实时处理部分的节点完成！")
      childrens = client.getChildren().forPath(zkTopicPath)//不会处理if else的关系，所以再获取一次
    }
    val offSets: mutable.Buffer[(TopicPartition, Long)] = for {
      p <- childrens  //这里需要import scala.collection.JavaConversions._
    }
      yield {
        println(p)
        val offsetData = client.getData().forPath(s"$zkTopicPath/$p")
        val offSet = java.lang.Long.valueOf(new String(offsetData)).toLong
        // 返回  (TopicPartition, Long)
        (new TopicPartition(topic, Integer.parseInt(p)), offSet)
      }
    println(offSets.toMap)
    offSets.toMap
  }
}