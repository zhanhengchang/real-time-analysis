如果Spark连接MySQL用户授权之后还一直报
        “Cannot create PoolableConnectionFactory Access denied for user 'root'@'localhost'”的话，
可能是集群压力太大，重启集群重新运行。
        还报这个异常的话，登录MySQL，运行命令重新进行授权。
        GRANT ALL PRIVILEGES ON *.* TO root@'%' IDENTIFIED BY 'root' WITH GRANT OPTION;
        FLUSH PRIVILEGES;
然后重启MySQL
        service mysql restart